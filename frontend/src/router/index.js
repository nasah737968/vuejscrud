// import from vue router
import { createRouter, createWebHistory } from 'vue-router'

// define route
const routes = [
    {
        path: '/',
        name: 'student.index',
        component: () => import('@/views/student/Index.vue')
    },
    {
        path: '/create',
        name: 'student.create',
        component: () => import('@/views/student/Create.vue')
    },
    {
        path: '/edit/:id',
        name: 'student.edit',
        component: () => import('@/views/student/Edit.vue')
    }
]

// create router
const router = createRouter({
    history: createWebHistory(), routes //config routes
})

export default router