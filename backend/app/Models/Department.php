<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $table = 'departments';
    
    protected $fillable = [
        'id', 'student_id', 'department', 'created_at', 'updated_at'
    ];
}
