<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Student;
use App\Models\Department;
use App\Models\Result;
use DB;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $students = DB::table('students')
                    ->join('departments', 'students.id', '=', 'departments.student_id')
                    ->join('results', 'students.id', '=', 'results.student_id')
                    ->select(
                        'students.id',
                        'students.name',
                        'students.age',
                        'students.mobile_no',
                        'departments.id as dept_pk_id',
                        'departments.student_id as dept_fk_id',
                        'departments.department',
                        'results.id as result_pk_id',
                        'results.student_id as result_fk_id',
                        'results.cgpa'
                    )
                    ->get();

        if ($request->name) {
            $students = $students->where('students.name', $request->name);
        }

        if ($request->department) {
            $students = $students->where('departments.department', $request->department);
        }

        if ($request->cgpa) {
            $students = $students->where('results.cgpa', $request->cgpa);
        }

        $list = $students;

        if (!$list) {
            return response()->json([
                'success' => false,
                'message' => 'Failed to get List Data'
            ], 400);            
        }

        return response()->json([
            'success' => true,
            'message' => 'List Data Student',
            'data' => $list
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'age' => 'required',
            'mobile_no' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        };

        $student = Student::create([
            'name' => $request->name,
            'age' => $request->age,
            'mobile_no' => $request->mobile_no
        ]);
        $department = Department::create([
            'student_id' => $student->id,
            'department' => $request->department
        ]);
        $result = Result::create([
            'student_id' => $student->id,
            'cgpa' => $request->cgpa
        ]);

        if ($student) {
            return response()->json([
                'success' => true,
                'message' => 'Student Created Successfully',
                'data' => $student,
                'data2' => $department,
                'data3' => $result
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Student Data Failed!'
        ], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = DB::table('students')
                    ->join('departments', 'students.id', '=', 'departments.student_id')
                    ->join('results', 'students.id', '=', 'results.student_id')
                    ->select(
                        'students.id',
                        'students.name',
                        'students.age',
                        'students.mobile_no',
                        'departments.id as dept_pk_id',
                        'departments.student_id as dept_fk_id',
                        'departments.department',
                        'results.id as result_pk_id',
                        'results.student_id as result_fk_id',
                        'results.cgpa'
                    )
                    ->where('students.id', $id)
                    ->first();

        if (!$student) {
            return response()->json([
                'success' => false,
                'message' => "ID" . $id . 'Failed to get Student Data'
            ], 400);            
        }
        
        return response()->json([
            'success' => true,
            'message' => "ID" . $id . 'Student Data Get Successfully',
            'data' => $student
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'age' => 'required',
            'mobile_no' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        };

        $student = Student::findOrFail($id);
        $department = Department::where('student_id', $id)->first();
        $result = Result::where('student_id', $id)->first();

        if ($student) {
            $student->name = $request->name;
            $student->age = $request->age;
            $student->mobile_no = $request->mobile_no;
            $student->save();

            $department->student_id = $student->id;
            $department->department = $request->department;
            $department->save();

            $result->student_id = $student->id;
            $result->cgpa = $request->cgpa;
            $result->save();
            
            return response()->json([
                'success' => true,
                'message' => "ID" . $id . 'Data Updated Successfully',
                'data' => $student,
                'data' => $department,
                'data' => $result
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => "ID" . $id . 'Data Update Failed!'
        ], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::findOrFail($id)->delete();

        if (!$student) {
            return response()->json([
                'success' => false,
                'message' => 'Failed to Delete Data'
            ], 400);            
        }

        return response()->json([
            'success' => true,
            'message' => "ID" . $id . 'Student Deleted Successfully'
        ], 200);
    }
}
